package com.hibernate.ms9hibernate;

import com.hibernate.ms9hibernate.entity.Adress;
import com.hibernate.ms9hibernate.entity.Contact;
import com.hibernate.ms9hibernate.entity.Role;
import com.hibernate.ms9hibernate.entity.User;
import com.hibernate.ms9hibernate.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Set;

@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class Ms9HibernateApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Ms9HibernateApplication.class, args);
	}

	private final UserRepository userRepository;


	@Override
	public void run(String... args) throws Exception{

		User user = new User();
		Adress adress = new Adress();
		Contact contact1 = new Contact();
		Contact contact2 = new Contact();

		Role role1= new Role();
		Role role2= new Role();


		contact1.setContactNumber("123456");
		contact2.setContactNumber("1234567");
		Set<Contact> contacts = Set.of(contact1,contact2);


		adress.setContacts(contacts);
		adress.setName("Krakow");


		role1.setRole("Manager");
		role2.setRole("Reader");

		Set<Role> roles = Set.of(role1,role2);

		user.setUserRoles(roles);
		user.setUsername("Araz");
		user.setAddress(adress);
		user.setPassword("password");


		userRepository.save(user);


	}
}
