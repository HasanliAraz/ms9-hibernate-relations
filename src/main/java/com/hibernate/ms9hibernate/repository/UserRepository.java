package com.hibernate.ms9hibernate.repository;

import com.hibernate.ms9hibernate.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
